# Grocery planner
Meteor application for grocery lists organisation

### Prerequisites
- Meteor 1.4
- Robomongo to browse database (optional)

### How to run
```$ meteor --settings=settings.json```